# ADP Task Test

It's a math terminal that you should answer math calcules

just run

```shell
$ npm install && npm start
```

and open [http://localhost:4000](http://localhost:4000)

### Instruction to play

1 - type `start`

2 - then you will see the `id` and the `Question`

3 - type the `id` and the answer, like this `61a2b8c5-2bae-4586-a4a0-a8a40142fd58 3120654974041068`

If the answer is correct, we will raffle another one for you to answer, otherwise, keep trying or give a `skip`

You can type `reset` to reset :)

### Stack

- NodeJs web server build from scratch;

- Javascript client SPA build from scratch too;

hope you like it
