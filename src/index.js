import server from './server'
import settings from './settings'
import * as routes from './routes'

import { logger } from './utils'

server.get('/public/:fileName', routes.staticFiles)

server.get('/', routes.index)
server.get('/get-task', routes.getTask)
server.post('/submit-task', routes.submitTask)
server.get('/fake', routes.fake)

const start = async () => {
  try {
    await server.listen(settings.appPort)
    logger.info('The HTTP server is running on port', settings.appPort)
  } catch (error) {
    logger.error('WHOOOOOOPS', error)
  }
}

start()
