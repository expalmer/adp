import fs from 'fs'
import mime from 'mime'
import pino from 'pino'

const publicFolder = `${__dirname}/public`

export const logger = pino({
  prettyPrint: { colorize: true },
})

export const getStaticFile = file =>
  new Promise(resolve => {
    fs.readFile(`${publicFolder}/${file}`, (err, buffer) => {
      if (err) resolve('')

      resolve(buffer)
    })
  })

export const getPayloadString = (payload, contentType) => {
  const payloadString = payload || ''
  return contentType === 'json' ? JSON.stringify(payloadString) : payloadString
}

export const getMimeType = contentType => {
  return mime.getType(contentType)
}

export const wait = (ms = 1000) =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, ms)
  })
