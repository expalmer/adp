import http from 'http'
import findMyWay from 'find-my-way'
import { StringDecoder } from 'string_decoder'
import parseUrl from 'parseurl'
import { decode } from 'qss'

import { getMimeType, getPayloadString } from './utils'

function responseHandler({
  res,
  payload = '',
  statusCode = 200,
  contentType = 'json',
} = {}) {
  const payloadString = getPayloadString(payload, contentType)
  const headerContentType = getMimeType(contentType)

  res.setHeader('Content-Type', headerContentType)

  res.writeHead(statusCode)
  res.end(payloadString)
}

const router = findMyWay({
  defaultRoute: (req, res) => {
    responseHandler({
      res,
      payload: { message: 'NOT FOUND' },
      statusCode: 404,
    })
  },
  onBadUrl: (path, req, res) => {
    responseHandler({
      res,
      payload: { message: `BAD URL: ${path}` },
      statusCode: 400,
    })
  },
})

const routeHandler = handler => (req, res, params) => {
  const decoder = new StringDecoder('utf-8')
  const urlParsed = parseUrl(req)
  const { query: queryString } = urlParsed

  let buffer = ''

  req.on('data', data => {
    buffer += decoder.write(data)
  })

  req.on('end', () => {
    buffer += decoder.end()
    const request = {
      ...req,
      params,
      query: queryString ? decode(queryString) : undefined,
      body: buffer ? JSON.parse(buffer) : undefined,
    }

    handler(request, ({ statusCode, payload, contentType }) => {
      responseHandler({
        res,
        payload,
        statusCode,
        contentType,
      })
    })
  })
}

const addRoute = (method, path, handler) => {
  router.on(method, path, routeHandler(handler))
}

const get = (path, handler) => addRoute('GET', path, handler)
const post = (path, handler) => addRoute('POST', path, handler)

const listen = port =>
  new Promise((resolve, reject) => {
    try {
      const server = http.createServer((req, res) => {
        router.lookup(req, res)
      })
      server.listen(port, resolve)
    } catch (error) {
      reject(error)
    }
  })

export default {
  listen,
  get,
  post,
}
