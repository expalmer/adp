const { APP_PORT, ADP_API } = process.env

export default {
  appPort: APP_PORT,
  adpApi: ADP_API,
}
