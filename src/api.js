import axios from 'axios'

import settings from './settings'
import { wait } from './utils'

export const getTask = () => axios.get(`${settings.adpApi}/v1/get-task`)

export const submitTask = ({ id, result }) =>
  axios.post(`${settings.adpApi}/v1/submit-task`, { id, result })

export const fakeTask = async (ms = 400) => {
  await wait(ms)
  return {
    id: '123',
    operation: 'subtraction',
    left: 10,
    right: 20,
  }
}
