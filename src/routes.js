import * as api from './api'
import { getStaticFile } from './utils'

export const fake = async (req, reply) => {
  const data = await api.fakeTask(300)
  reply({
    statusCode: 200,
    payload: data,
  })
}

export const index = (req, reply) => {
  getStaticFile('index.html').then(html => {
    reply({
      contentType: 'html',
      statusCode: 200,
      payload: html,
    })
  })
}

export const getTask = async (req, reply) => {
  try {
    const { data } = await api.getTask()

    reply({
      statusCode: 200,
      payload: data,
    })
  } catch (error) {
    const { response: { status = 500, data = '' } = {} } = error || {}
    reply({
      statusCode: status,
      payload: { status, data },
    })
  }
}

export const submitTask = async (req, reply) => {
  try {
    const { body } = req
    const { data } = await api.submitTask(body)

    reply({
      statusCode: 200,
      payload: { status: 200, data },
    })
  } catch (error) {
    const { response: { status = 500, data = '' } = {} } = error || {}
    reply({
      statusCode: status,
      payload: { status, data },
    })
  }
}

export const staticFiles = (req, reply) => {
  const {
    params: { fileName },
  } = req
  const file = fileName.replace('public/', '').trim()

  getStaticFile(file).then(html => {
    reply({
      contentType: file,
      statusCode: html ? 200 : 404,
      payload: html,
    })
  })
}
