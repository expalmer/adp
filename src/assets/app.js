import './css/style.css'

import { stages, commands } from './js/constants'

import createStore from './js/store'
import createActions from './js/actions'
import { renders, renderTotal } from './js/renders'

const store = createStore({
  stage: stages.INITIAL,
  placeholder: 'start',
  isLoading: false,
  items: [],
  timers: [],
  total: {
    correct: 0,
    wrong: 0,
    skip: 0,
  },
})

const actions = createActions(store)

/**
 * Handle Interaction
 */

const inputHandler = event => {
  const { value } = event.target

  const clearInput = () => {
    /* eslint no-param-reassign: "error" */
    event.target.value = ''
  }

  if (event.key === 'Enter') {
    const { stage } = store.getState()

    if (value === commands.HELP) {
      actions.help()
      clearInput()
      return
    }

    if (value === commands.SKIP) {
      actions.skip()
      clearInput()
      return
    }

    if (value === commands.RESET) {
      actions.reset()
      clearInput()
      return
    }

    if (stage === stages.INITIAL && value === commands.START) {
      actions.start()
      clearInput()
      return
    }

    if (stage === stages.WAITING_ANSWER && value.split(' ').length === 2) {
      actions.answerQuestion(value)
      clearInput()
      return
    }

    actions.wrong(value)
    clearInput()
  }
}

/**
 * Iniatialize App
 */

function ready() {
  const $screen = document.querySelector('#js-screen')
  const $input = document.querySelector('#js-input')
  const $items = document.querySelector('#js-items')
  const $total = document.querySelector('#js-total')

  $input.focus()
  $input.addEventListener('keyup', inputHandler)

  const renderItems = () => {
    const { items, placeholder, total } = store.getState()

    $input.setAttribute('placeholder', placeholder)

    // eslint-disable-next-line unicorn/no-array-reduce
    const htmlItems = items.reduce(
      (acc, item) => `${acc}${renders[item.type](item)}`,
      '',
    )

    // eslint-disable-next-line unicorn/no-array-reduce
    const htmlTotal = Object.entries(total).reduce(
      (acc, [key, val]) => `${acc}${renderTotal({ key, val })}`,
      '',
    )

    $items.innerHTML = htmlItems
    $total.innerHTML = htmlTotal

    window.scroll({
      top: $screen.scrollHeight,
      left: 0,
      behavior: 'smooth',
    })
  }

  store.subscribe(renderItems)
  store.dispatch({})
}

document.addEventListener('DOMContentLoaded', ready)
