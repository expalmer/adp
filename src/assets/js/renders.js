import { types, colors, operations } from './constants'

const line = x => `<span>~ </span>${x}`

const span = (text, color = 'white') => `<span class="${color}">${text}</span>`

const renderLine = ({ text, color = '', isInput }) => `
 <p>${isInput ? line(span(text, color)) : span(text, color)}</p>
 `

const renderQuestion = ({ id, left, right, operation }) => `
 <p>${span('Id: ')}${span(id, colors.YELLOW)}</p>
 <p>
   ${span('Question: ')}
   ${span(left, colors.YELLOW)}
   ${span(' ')}${span(operations[operation] || operation, colors.PINK)}
   ${span(' ')}
   ${span(right, colors.YELLOW)}
 </p>
  `

const renderAnswer = ({ data, status }) =>
  `<p><span class="green">${status}: ${data}</span></p>`

export const renders = {
  [types.LINE]: renderLine,
  [types.QUESTION]: renderQuestion,
  [types.ANSWER]: renderAnswer,
}

export const renderTotal = ({ key, val }) => `
     <div class="total__item ${key}">${key}: ${val}</div>
   `
