const ajax = async (method, url, body) => {
  const API = 'http://localhost:4000'
  const options = {
    credentials: 'same-origin',
    method,
    headers: { 'Content-Type': 'application/json' },
  }

  if (body) {
    options.body = JSON.stringify(body)
  }

  const response = await fetch(`${API}/${url}`, options)

  const { ok, status } = response
  const json = await response.json()
  const payload = { status, json }

  if (ok) {
    return payload
  }

  return Promise.reject(payload)
}

const api = {
  getTask: async () => ajax('GET', 'get-task'),
  submitTask: async (id = '', result = 0) =>
    ajax('POST', 'submit-task', { id, result }),
}

export default api
