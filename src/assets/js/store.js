const createStore = (initialState = {}) => {
  let state = initialState
  const subcriptions = []

  const dispatch = nextState => {
    state = { ...state, ...nextState }
    /* eslint-disable-next-line unicorn/no-array-for-each */
    subcriptions.forEach(fn => fn())
  }

  const subscribe = fn => subcriptions.push(fn)

  return {
    dispatch,
    subscribe,
    getState: () => ({ ...state }),
  }
}

export default createStore
