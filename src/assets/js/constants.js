export const stages = {
  INITIAL: 'INITIAL',
  WAITING_ANSWER: 'WAITING_ANSWER',
}

export const types = {
  LINE: 'LINE',
  QUESTION: 'QUESTION',
  ANSWER: 'ANSWER',
}

export const colors = {
  RED: 'red',
  GREEN: 'green',
  PINK: 'pink',
  YELLOW: 'yellow',
  GRAY: 'gray',
}

export const commands = {
  START: 'start',
  RESET: 'reset',
  SKIP: 'skip',
  HELP: 'help',
}

export const operations = {
  addition: '+',
  subtraction: '-',
  division: '/',
  remainder: '%',
  multiplication: '*',
}
