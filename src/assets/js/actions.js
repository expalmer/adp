import { stages, types, colors } from './constants'
import api from './api'

const createActions = store => {
  const addLine = ({ text, color, isInput = false }) => {
    store.dispatch({
      items: [
        ...store.getState().items,
        {
          type: types.LINE,
          text,
          color,
          isInput,
        },
      ],
    })
  }

  const requestQuestion = async () => {
    const { stage, isLoading } = store.getState()

    if (isLoading || ![stages.INITIAL].includes(stage)) {
      return
    }

    addLine({
      text: 'wait, I`m featching a question to you...',
      color: colors.GRAY,
    })

    store.dispatch({
      isLoading: true,
    })

    api
      .getTask()
      .then(({ json }) => {
        const item = {
          ...json,
          type: types.QUESTION,
        }

        store.dispatch({
          stage: stages.WAITING_ANSWER,
          isLoading: false,
          placeholder: `${item.id} 8072388567682993`,
          items: [...store.getState().items, item],
        })

        addLine({
          text: 'type the `id` followed by the `result` as below',
          color: colors.GRAY,
        })
      })
      .catch(({ status, json }) => {
        addLine({
          color: colors.RED,
          text: `${status}: ${json.data}`,
        })

        store.dispatch({
          isLoading: false,
        })
      })
  }

  const answerQuestion = value => {
    const [id, result] = value.split(' ')
    const { stage, timers, isLoading } = store.getState()

    if (isLoading || stage !== stages.WAITING_ANSWER) {
      return
    }

    addLine({
      text: value,
      isInput: true,
    })

    store.dispatch({
      isLoading: true,
    })

    api
      .submitTask(id, +result)
      .then(({ json }) => {
        const item = {
          ...json,
          type: types.ANSWER,
        }

        const callMeLater = setTimeout(() => {
          requestQuestion()
        }, 1000)

        const { total, items } = store.getState()
        store.dispatch({
          stage: stages.INITIAL,
          isLoading: false,
          items: [...items, item],
          timers: [...timers, callMeLater],
          total: { ...total, correct: total.correct + 1 },
        })
      })
      .catch(({ status, json }) => {
        const { total } = store.getState()

        addLine({
          color: colors.RED,
          text: `${status}: ${json.data}`,
        })

        const isWrongAnswer = status === 400

        store.dispatch({
          isLoading: false,
          total: isWrongAnswer ? { ...total, wrong: total.wrong + 1 } : total,
        })
      })
  }

  const skip = () => {
    const { stage } = store.getState()

    addLine({
      text: 'skip',
      isInput: true,
    })

    if (stage !== stages.WAITING_ANSWER) {
      addLine({
        text: 'you cant do that, type `start`',
        color: colors.GRAY,
      })
      return
    }

    addLine({
      text: `Ok... try again`,
      color: colors.PINK,
    })

    const callMeLater = setTimeout(() => {
      requestQuestion()
    }, 100)

    const { timers, total } = store.getState()

    store.dispatch({
      stage: stages.INITIAL,
      timers: [...timers, callMeLater],
      total: { ...total, skip: total.skip + 1 },
    })
  }

  const wrong = value => {
    addLine({ text: value, isInput: true })
    addLine({ text: `command not found: ${value}`, color: colors.RED })
  }

  const start = () => {
    const { stage } = store.getState()

    if (stage !== stages.INITIAL) {
      return
    }

    const callMeLater1 = setTimeout(() => {
      addLine({ text: 'Let`s Go!', color: colors.PINK })
    }, 100)

    const callMeLater2 = setTimeout(() => {
      requestQuestion()
    }, 700)

    addLine({ text: 'start', isInput: true })

    store.dispatch({
      timers: [callMeLater1, callMeLater2],
    })
  }

  const reset = () => {
    const { timers } = store.getState()

    /* eslint-disable-next-line unicorn/no-array-for-each */
    timers.forEach(timer => clearTimeout(timer))

    store.dispatch({
      stage: stages.INITIAL,
      isLoading: false,
      timers: [],
      placeholder: 'start',
      items: [],
    })
  }

  const help = () => {
    addLine({ text: 'help' })
    addLine({ text: 'start           to start the game' })
    addLine({ text: 'reset           to reset the game' })
    addLine({ text: 'skip            to skip an question' })
  }

  return {
    start,
    reset,
    help,
    wrong,
    skip,
    answerQuestion,
    requestQuestion,
  }
}

export default createActions
