import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import postcss from 'rollup-plugin-postcss'
import precss from 'precss'
import autoprefixer from 'autoprefixer'
import cssnano from 'cssnano'

const production = !process.env.ROLLUP_WATCH

export default {
  input: 'src/assets/app.js',
  output: {
    file: 'src/public/bundle.js',
    format: 'iife',
    sourcemap: false,
  },
  plugins: [
    resolve(),
    commonjs(),
    production && terser(),
    postcss({
      extract: true,
      plugins: [precss, autoprefixer, cssnano],
    }),
  ],
}
